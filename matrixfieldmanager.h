#ifndef MATRIXFIELDMANAGER_H
#define MATRIXFIELDMANAGER_H

#include "node.h"
#include "utils.h"
#include <QVector>

class MatrixFieldManager
{
public:
    MatrixFieldManager();

    Node* AlienHQ;

    Node* HumanHQ;

    Graph* GMatrix;

    QVector<QVector<Node*>> *NodesMatrix;

    int Rows, Cols;

    void CreateMatrix(int rows, int cols, float hiddenPerc, float emptyPerc);

    bool IsTileAccessible(int row, int col);

};

#endif // MATRIXFIELDMANAGER_H
