#ifndef AI_H
#define AI_H

#include "matrixfieldmanager.h"
#include <QDialog>
#include <QList>

enum Action 
{
	MOVE,
	DECLARE,
	ATTACK
};


enum RoleTypes
{
    ALIEN,
    HUMAN
};


class AI
{
public:
    AI(RoleTypes r, MatrixFieldManager *mfm);

    void Move(Node* uiPos);
    void Declare();

    Node* CurrentPosition;
    Node* FakePosition;

private:
    QList<Node*> FMax,FMin;

    Node* D_FMax,* D_FMin;

    // Punti sulla base dei quali viene calcolata la direzione
    QList<Node*> D_FMaxPoints,D_FMinPoints;

    Action CurrentAction;

    MatrixFieldManager* MFM;

    QList<Node*> EnemyPositions;

    RoleTypes Role;

    // numero di mosse FMin effettuate
    int Cnt_FMin = 0;

    Node* GoTo(Node* dest, Node* direction, Node* start);

    void UpdateFrontier(QList<Node*>* F);

    void InvertMinMax(bool clearFmin=true);

    void UpdateDirection(Node* direction, QList<Node*>* queue);


};

#endif // AI_H
