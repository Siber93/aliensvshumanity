#include "gamemanager.h"

#include <QMessageBox>

GameManager::GameManager(MatrixFieldManager* fm)
{
    MFM = fm;
    moovesCounter = 2*qMax(MFM->Rows,MFM->Cols);
}


bool GameManager::MakeMove(RoleTypes r, Node* n)
{

    // Pesco una carta e vedo che cosa deve succedere

    quint32 value = QRandomGenerator::global()->generate();
    if(value % 2)
    {
        // Carta verde
        return true;
    }
    else
    {
        // Carta rossa
        return false;
    }

}


Node* GameManager::MakeDeclaration(RoleTypes r, Node* n, Node* cpos)
{
    moovesCounter--;
    QMessageBox msgBox;
    msgBox.setText("GameOver");
    msgBox.setInformativeText("AI wins");

    if(cpos->IsShuttle)
    {
        msgBox.setText("GameOver");
        msgBox.setInformativeText("You win!!");
        msgBox.exec();
        return nullptr;
    }

    if(cpos == ai->CurrentPosition || moovesCounter ==0)
    {
        msgBox.exec();
    }
    // Muovo la AI
    ai->Move(n);

    if(cpos == ai->CurrentPosition)
    {
        msgBox.exec();
    }
    // Controllo se l'ai e' su una casella grigia
    if(ai->CurrentPosition->IsHidden)
    {
        return nullptr;
    }

    quint32 value = QRandomGenerator::global()->generate();
    QString c = "Real: " + QString::number(ai->CurrentPosition->ColID) + "," + QString::number(ai->CurrentPosition->RowID) + "           Fake: " + QString::number(ai->FakePosition->ColID) + "," + QString::number(ai->FakePosition->RowID) ;
    qInfo( c.toUtf8() );
    if(value % 2)
    {
        // Carta verde
        return ai->FakePosition;
    }
    else
    {
        // Carta rossa
        return ai->CurrentPosition;
    }

}


bool GameManager::IsTileAccessible(RoleTypes t, Node* currPos, int r, int c)
{
    // Non deve essere la stessa posizione di quella attuale
    if (r== currPos->RowID &&
        c== currPos->ColID)
    {
        return false;
    }

    // Deve essere all'interno del range raggiungibile da questo player
    if (r> currPos->RowID+1 || r< currPos->RowID-1 ||
        c> currPos->ColID+1 || c< currPos->ColID-1)
    {
        return false;
    }
    return MFM->IsTileAccessible(r,c);
}

