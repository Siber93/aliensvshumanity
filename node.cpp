#include "node.h"
#include <QtMath>

Node::Node(int row, int col)
{
    RowID = row;
    ColID = col;
    IsHidden = false;
    IsDeleted = false;
    IsAlienBase = false;
    IsHumanBase = false;
    IsShuttle = false;
    drawingScaleFactor = 10;
}

void Node::MarkHidden()
{
    IsHidden = true;
}


void Node::MarkAsHumanBase()
{
    IsHumanBase = true;
    IsDeleted = false;
    IsHidden = false;
    IsAlienBase = false;
    IsShuttle = false;
}

void Node::MarkAsAlienBase()
{
    IsHumanBase = false;
    IsDeleted = false;
    IsHidden = false;
    IsAlienBase = true;
    IsShuttle = false;
}

void Node::MarkAsShuttle()
{
    IsHumanBase = false;
    IsDeleted = false;
    IsHidden = false;
    IsAlienBase = false;
    IsShuttle = true;
}

void Node::MarkRevealed()
{
    IsHidden = false;
}

QPointF** Node::GetGraphicPoints()
{
    QPointF* result[6];

    // Start with the leftmost corner of the upper left hexagon.
    float width = HexWidth(drawingScaleFactor);
    float y = drawingScaleFactor / 2;
    float x = 0;

    // Move down the required number of rows.
    y += RowID * drawingScaleFactor;

    // If the column is odd, move down half a hex more.
    if (ColID % 2 == 1) y += drawingScaleFactor / 2;

    // Move over for the column number.
    x += ColID * (width * 0.75f);

    // Generate the points.

    result[0] = new QPointF(x, y);
    result[0] = new QPointF(x + width * 0.25f, y - drawingScaleFactor / 2);
    result[0] = new QPointF(x + width * 0.75f, y - drawingScaleFactor / 2);
    result[0] = new QPointF(x + width, y);
    result[0] = new QPointF(x + width * 0.75f, y + drawingScaleFactor / 2);
    result[0] = new QPointF(x + width * 0.25f, y + drawingScaleFactor / 2);

    return result;

}

float Node::HexWidth(float height)
{
    return (float)(4 * (height / 2 / qSqrt(3)));
}
