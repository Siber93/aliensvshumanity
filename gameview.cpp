#include "gameview.h"
#include "ui_gameview.h"

#include <QMessageBox>


GameView::GameView(GameManager *gm, RoleTypes r, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::GameView)
{
    ui->setupUi(this);    
    GM = gm;
    CurrentAction = MOVE;
    if(r==HUMAN)
    {
        CurrentPosition = GM->MFM->HumanHQ;
    }
    else {
        CurrentPosition = GM->MFM->AlienHQ;
    }
    Role = r;

    QVector<QVector<Node*>> *mn = GM->MFM->NodesMatrix;

    Buttons = new QVector<QVector<TileButton*> >(mn->length(), QVector<TileButton*>(mn[0].length()));
}

GameView::~GameView()
{
    delete ui;
}

void GameView::UpdateGrid()
{

    QVector<QVector<Node*>> *mn = GM->MFM->NodesMatrix;

    // Pulisco layout
    ui->moovesCounter->display(GM->moovesCounter);

    // Ridisegno tutto
    for(int i = 0; i < mn->length(); i++)
    {
        for(int j = 0; j < mn[0].length(); j++)
        {
            bool draw = false;
            // Controllo se il bottone era gia stato disegnato, nel caso devo solo aggiornarelo stile
            if((*Buttons)[i][j] == nullptr)
            {
                TileButton *cell = new TileButton(i,j,this);
                cell->setText(QString::number(j)+","+QString::number(i));
                (*Buttons)[i][j] = cell;
                draw = true;
            }
            if(CurrentAction != DECLARE)
            {
                (*Buttons)[i][j]->setEnabled(false);
            }
            else
            {
                (*Buttons)[i][j]->setEnabled(true);
            }
            // Azzero lo stile
            QString qss = QString("");
            (*Buttons)[i][j]->setStyleSheet(qss);

            // Controllo se e' la cella in cui sono ora
            if(i == CurrentPosition->RowID && j == CurrentPosition->ColID)
            {
              qss+= "border-width: 2px; border-style: solid; border-color: yellow;";
               (*Buttons)[i][j]->setStyleSheet(qss);
            }

            // Controllo se il bottono e' stato segnalato dall ai nemica
            for(int l = 0; l < EnemyPositions.length(); l++)
            {
                if(EnemyPositions.at(l)== nullptr)
                    continue;

                if(i==EnemyPositions.at(l)->RowID && j==EnemyPositions.at(l)->ColID)
                {
                    if(l==EnemyPositions.length()-1)
                    {
                        qss+= "border-width: 2px; border-style: solid; border-color: orange;";
                    }
                    else
                    {
                        qss+= "border-width: 2px; border-style: solid; border-color: red;";
                    }
                }
            }

            if(!(*mn)[i][j]->IsDeleted)
            {
                if((*mn)[i][j]->IsHidden)
                {
                    // Caso cella nascosta, dove non e' necessario rivelare posizione
                    QColor col = QColor(Qt::gray);
                    if(col.isValid()) {
                       qss += QString("background-color: %1").arg(col.name());
                       (*Buttons)[i][j]->setStyleSheet(qss);
                    }
                }
                else if((*mn)[i][j]->IsShuttle)
                {
                    // Caso cella navicella
                    QColor col = QColor(Qt::yellow);
                    if(col.isValid()) {
                       qss += QString("background-color: %1").arg(col.name());
                       (*Buttons)[i][j]->setStyleSheet(qss);
                    }
                }
                else if ((*mn)[i][j]->IsAlienBase)
                {
                    // Caso cella base aliena
                    QColor col = QColor(Qt::green);
                    if(col.isValid()) {
                       qss += QString("background-color: %1").arg(col.name());
                       (*Buttons)[i][j]->setStyleSheet(qss);
                    }
                }
                else if ((*mn)[i][j]->IsHumanBase)
                {
                    // Caso cella base umana
                    QColor col = QColor(Qt::blue);
                    if(col.isValid()) {
                       qss += QString("background-color: %1").arg(col.name());
                       (*Buttons)[i][j]->setStyleSheet(qss);
                    }
                }
                else
                {
                    // Caso cella normale
                    QColor col = QColor(Qt::white);
                    if(col.isValid()) {
                       qss += QString("background-color: %1").arg(col.name());
                       (*Buttons)[i][j]->setStyleSheet(qss);
                    }
                }
            }
            else
            {
                // Caso in cui la cella e' vuota
                QColor col = QColor(Qt::black);
                if(col.isValid()) {
                   qss += QString("background-color: %1").arg(col.name());
                   (*Buttons)[i][j]->setStyleSheet(qss);
                }
            }

            if(GM->IsTileAccessible(Role, CurrentPosition, i,j))
            {
                (*Buttons)[i][j]->setEnabled(true);                
            }

            if(draw)
            {
                connect((*Buttons)[i][j], SIGNAL(clicked()), this, SLOT(MakeInteractiveMove()));
                ui->mainLayout->addWidget((*Buttons)[i][j],i,j);                
            }

        }
    }
    //ui->mainLayout->invalidate();
}

void GameView::ClearWidgets(QLayout * layout, int numitem) {
   if (! layout)
      return;
   for(int i = 0; i<numitem; i++)
   {
        delete layout->takeAt(0)->widget();
   }
}



void GameView::MakeInteractiveMove(void)
{
   TileButton* button = qobject_cast<TileButton*>(sender());
   if( button != nullptr )
   {
       Node* m = (*GM->MFM->NodesMatrix)[button->Row][button->Col];
       switch (CurrentAction)
       {
        case MOVE:
        {
           QMessageBox msgBox;
           if(m->IsHidden)
           {
               EnemyPositions.append(GM->MakeDeclaration(Role, nullptr, m));
               CurrentAction = MOVE;

           }
           else
           {
               if(!GM->MakeMove(Role, m))
               {
                   EnemyPositions.append(GM->MakeDeclaration(Role, m, m));
                   CurrentAction = MOVE;

                   msgBox.setText("Red card drawn");
                   msgBox.setInformativeText(":(");
               }
               else
               {
                   CurrentAction = DECLARE;

                   msgBox.setText("Green card drawn");
                   msgBox.setInformativeText("Declare a position");

               }

               msgBox.setStandardButtons(QMessageBox::Ok);
               int ret = msgBox.exec();
           }

           CurrentPosition = m;

           break;
       }
       case DECLARE:
       {
           EnemyPositions.append(GM->MakeDeclaration(Role, m, CurrentPosition));
           CurrentAction = MOVE;           
          break;
       }
       case ATTACK:
          break;
       }
       UpdateGrid();
   }
}
