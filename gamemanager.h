#ifndef GAMEMANAGER_H
#define GAMEMANAGER_H


#include "ai.h"

#include<QRandomGenerator>



class GameManager
{
public:

    AI* ai;
    MatrixFieldManager* MFM;

    int moovesCounter = 0;
    GameManager(MatrixFieldManager* fm);
    bool MakeMove(RoleTypes r, Node* n);
    Node* MakeDeclaration(RoleTypes r, Node* n, Node* cpos);
    bool IsTileAccessible(RoleTypes t, Node* currPos, int r, int c);
};

#endif // GAMEMANAGER_H
