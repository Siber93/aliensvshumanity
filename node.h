#ifndef NODE_H
#define NODE_H


#include <QPointF>

class Node
{
public:
    Node(int row, int col);

    int RowID;

    int ColID;

    bool IsHidden;

    bool IsDeleted;

    bool IsAlienBase;

    bool IsHumanBase;

    bool IsShuttle;

    float drawingScaleFactor;


    void MarkHidden();
    void MarkRevealed();
    void MarkAsAlienBase();
    void MarkAsHumanBase();
    void MarkAsShuttle();


    QPointF** GetGraphicPoints();

private:
    float HexWidth(float height);
};

#endif // NODE_H
