#ifndef GAMEVIEW_H
#define GAMEVIEW_H

#include "gamemanager.h"
#include "tilebutton.h"

#include <QDialog>
#include <QList>
#include <QLCDNumber>

namespace Ui {
class GameView;
}

class GameView : public QDialog
{
    Q_OBJECT

public:
    explicit GameView(GameManager *gm, RoleTypes r, QWidget *parent = nullptr);
    ~GameView();
    void UpdateGrid();

private:

    QLCDNumber* mooves;

    QVector<QVector<TileButton*>>* Buttons;

    RoleTypes Role;

    Action CurrentAction;

    QList<Node*> EnemyPositions;

    Node* CurrentPosition;

    Ui::GameView *ui;

    GameManager* GM;

    void ClearWidgets(QLayout * layout, int numitem);
	

private slots:
    void MakeInteractiveMove(void);

};

#endif // GAMEVIEW_H
