#include "ai.h"
#include "QRandomGenerator"
#include <QtMath>

AI::AI(RoleTypes r, MatrixFieldManager *mfm)
{   
    Role = r;
    MFM = mfm;
    // Posizione corrente dell AI e' il suo HQ
    CurrentPosition = mfm->AlienHQ;
    FakePosition = mfm->AlienHQ;
    // FMax,Fmin inizialmente contengono HQ avversario
    FMax.append(mfm->HumanHQ);
    FMin.append(mfm->HumanHQ);

    // Aggiorno FMax e FMin
    UpdateFrontier(&FMax);
    UpdateFrontier(&FMin);

    // Aggiorno le direzione min max con posizione HQ nemico
    UpdateDirection(mfm->HumanHQ,&D_FMaxPoints);
    UpdateDirection(mfm->HumanHQ,&D_FMinPoints);

    /*D_FMax = {
        MFM->NodesMatrix->length()-CurrentPosition.row>CurrentPosition.row ? MFM->NodesMatrix->length(): 0,
        MFM->NodesMatrix->length()-CurrentPosition.col>CurrentPosition.row ? MFM->NodesMatrix->length():0};
    D_FMin = {D_FMax.row,D_FMax.col};*/
}


void AI::Move(Node* uiPos)
{    

    // Controllo se il giocatore e' entrato in una cella grigia
    if(uiPos == nullptr)
    {
        // Cella grigia
        // Cerco all'interno di FMax la cella grigia piu' vicina al goal direzione
        bool found = false;
        Node* X = FMax[0];
        int nX = X->ColID + X->RowID*(MFM->Cols);

        for(int i = 0; i < FMax.length(); i++){
            int nF = FMax[i]->ColID + FMax[i]->RowID*(MFM->Cols);
            int nD = D_FMax->ColID + D_FMax->RowID*(MFM->Cols);
            // Controllare se e' grigia e se potrebbe essere un nuovo minimo di distanza dal goal direzione
            if(FMax[i]->IsHidden
                    && MFM->GMatrix->MinCost(nF, nD)<=MFM->GMatrix->MinCost(nX,nD))
                    //&& d_x+d_y <= min_x+min_y)
            {
                // Aggiorno X con il nuovo minimo trovato
                X = FMax[i];
                nX = X->ColID + X->RowID*(MFM->Cols);
                found = true;
            }
        }

        // Verifico se ho trovato almeno una cella grigia all'interno di Fmax
        if(found)
        {

        }
        else
        {
            // Nessuna casella grigia trovata in FMax, provo in FMin
            X = FMin[0];
            int nX = X->ColID + X->RowID*(MFM->Cols);

            for(int i = 0; i < FMin.length(); i++){
                int nF = FMin[i]->ColID + FMin[i]->RowID*(MFM->Cols);
                int nD = D_FMin->ColID + D_FMin->RowID*(MFM->Cols);
                // Controllare se e' grigia e se potrebbe essere un nuovo minimo di distanza dal goal direzione
                if(FMin[i]->IsHidden
                        && MFM->GMatrix->MinCost(nF, nD)<=MFM->GMatrix->MinCost(nX,nD))
                        //&& d_x+d_y <= min_x+min_y)
                {
                    // Aggiorno X con il nuovo minimo trovato
                    X = FMin[i];
                    nX = X->ColID + X->RowID*(MFM->Cols);
                    found = true;
                }
            }

            // Se non l'ho trovato nemmeno ora vuol dire che si e' verificato un errore nell'algoritmo
            if(found)
            {
                // Caso trovato, inverto Fmin con FMax e svuoto Fmin
                InvertMinMax();
                Cnt_FMin = 0;

            }
            else {
                // Caso non trovato == errore
            }

        }
        // Vado verso X
        CurrentPosition = GoTo(X,D_FMax,CurrentPosition);
        // Aggiorno posizione fake
        Declare();

        // Inserisco dentro FMax tutte le celle grigie e aggiorno la frontiera
        int index = 0;
        while(index<FMax.length())
        {
            if(!FMax[index]->IsHidden)
            {
                FMax.removeAt(index);
                index = 0;
            }
            else {
                index++;
            }
        }
        if(CurrentPosition == X)
        {
            // Vuol dire che o il gioco e finito oppure X non va inserito in FMax
            FMax.removeOne(X);
        }


        // Aggiorno FMax, FMin
        UpdateFrontier(&FMax);
        UpdateFrontier(&FMin);


        return;
    }

    if(uiPos->IsHidden)
    {
        // Non e' possibile che una dichiarazione sia di tipo hidden
        // Cerco all'interno di FMax la cella non grigia piu' vicina al goal direzione
        bool found = false;
        Node* X = FMax[0];
        int nX = X->ColID + X->RowID*(MFM->Cols);

        for(int i = 0; i < FMax.length(); i++){
            int nF = FMax[i]->ColID + FMax[i]->RowID*(MFM->Cols);
            int nD = D_FMax->ColID + D_FMax->RowID*(MFM->Cols);
            // Controllare se non e' grigia e se potrebbe essere un nuovo minimo di distanza dal goal direzione
            if(!FMax[i]->IsHidden
                    && MFM->GMatrix->MinCost(nF, nD)<=MFM->GMatrix->MinCost(nX,nD))
                    //&& d_x+d_y <= min_x+min_y)
            {
                // Aggiorno X con il nuovo minimo trovato
                X = FMax[i];
                nX = X->ColID + X->RowID*(MFM->Cols);
                found = true;
            }
        }
        // Vado verso la posizione stimata
        CurrentPosition = GoTo(X,D_FMax, CurrentPosition);
        // Aggiorno posizione fake
        Declare();

        // Aggiorno FMax Fmin
        UpdateFrontier(&FMax);
        UpdateFrontier(&FMin);

        return;
    }

    // Controllo se uiPos e' in fmax
    if(FMax.contains(uiPos))
    {

        // Vado verso la posizione segnalata
        CurrentPosition = GoTo(uiPos,D_FMax, CurrentPosition);
        // Aggiorno posizione fake
        Declare();
        // Resetto contatore fmin
        Cnt_FMin = 0;

        // Verifico di essere effettivamente in uipos
        if(CurrentPosition == uiPos)
        {
            // Qui ho bisogno di eseguire un controllo se con questa mossa
            // ho terminato o no il gioco, nel caso il gioco prosegua  non ho ancora vinto,
            // quindi aggiorno la frontiera senza nodePos
            FMax.removeOne(uiPos);
            UpdateFrontier(&FMax);
            UpdateFrontier(&FMin);
        }
        else
        {
            // Considero uipos corretto

            // Aggiorno FMax Fmin
            FMax.clear();
            FMax.append(uiPos);
            UpdateFrontier(&FMax);
            UpdateFrontier(&FMin);
            // Aggiorno la direzione
            UpdateDirection(uiPos, &D_FMaxPoints);

        }

        return;

    }

    // Controllo se FMin e' vuoto
    if(FMin.length() == 0)
    {
        // Cerco all'interno di FMax la cella non grigia piu' vicina al goal direzione
        bool found = false;
        Node* X = FMax[0];
        int nX = X->ColID + X->RowID*(MFM->Cols);

        for(int i = 0; i < FMax.length(); i++){
            int nF = FMax[i]->ColID + FMax[i]->RowID*(MFM->Cols);
            int nD = D_FMax->ColID + D_FMax->RowID*(MFM->Cols);
            // Controllare se non e' grigia e se potrebbe essere un nuovo minimo di distanza dal goal direzione
            if(!FMax[i]->IsHidden
                    && MFM->GMatrix->MinCost(nF, nD)<=MFM->GMatrix->MinCost(nX,nD))
                    //&& d_x+d_y <= min_x+min_y)
            {
                // Aggiorno X con il nuovo minimo trovato
                X = FMax[i];
                nX = X->ColID + X->RowID*(MFM->Cols);
                found = true;
            }
        }
        // Vado verso la posizione stimata
        CurrentPosition = GoTo(X,D_FMax, CurrentPosition);
        // Aggiorno posizione fake
        Declare();
        // Incremento contatore fmin
        Cnt_FMin++;

        // Aggiorno FMax Fmin
        FMax.clear();
        FMax.append(X);
        UpdateFrontier(&FMax);
        FMin.append(uiPos);
        UpdateFrontier(&FMin);
        // Aggiorno la direzione
        UpdateDirection(uiPos, &D_FMinPoints);

    }
    else{
        // FMin non vuoto
        // Controllo se uiPos e' in fMin
        if(FMin.contains(uiPos))
        {
            // Incremento contatore fmin
            Cnt_FMin++;

            // Agiorno la direzione di Fmin
            UpdateDirection(uiPos, &D_FMinPoints);

            FMin.clear();
            FMin.append(uiPos);

            // Controllo se ho sforato il livello di soglia per switchare fmin con fmax
            if(Cnt_FMin >1)
            {
                InvertMinMax();

                CurrentPosition = GoTo(uiPos,D_FMax, CurrentPosition);
                // Aggiorno posizione fake
                Declare();
                // Aggiorno FMax Fmin
                UpdateFrontier(&FMax);
                UpdateFrontier(&FMin);
                return;
            }

            /*
            // Verifico di essere effettivamente in uipos
            if(CurrentPosition == uiPos)
            {
                // Qui ho bisogno di eseguire un controllo se con questa mossa
                // ho terminato o no il gioco, quindi alzo un flag di richiesta nel caso mi trovi proprio in uipos
                CheckGameOver =true;
                if(IsPosInF(&FMin, uiPos, &nodePos))
            }
            else
            {
                // Considero uipos corretto

                // Aggiorno FMax Fmin
                UpdateFrontier(&FMax);
                UpdateFrontier(&FMin);
                // Aggiorno la direzione
                UpdateDirection(uiPos, &D_FMaxPoints);

            }*/

        }

        // Se ha saltato la parte precedente uipos non e' in FMin quindi e' sicuramente \
        una dichiarazione falsa, procedo nella direzione currente su FMax

        // Cerco all'interno di FMax la cella non grigia piu' vicina al goal direzione
        bool found = false;
        Node* X = FMax[0];
        int nX = X->ColID + X->RowID*(MFM->Cols);

        for(int i = 0; i < FMax.length(); i++){
            int nF = FMax[i]->ColID + FMax[i]->RowID*(MFM->Cols);
            int nD = D_FMax->ColID + D_FMax->RowID*(MFM->Cols);
            // Controllare se non e' grigia e se potrebbe essere un nuovo minimo di distanza dal goal direzione
            if(!FMax[i]->IsHidden
                    && MFM->GMatrix->MinCost(nF, nD)<=MFM->GMatrix->MinCost(nX,nD))
                    //&& d_x+d_y <= min_x+min_y)
            {
                // Aggiorno X con il nuovo minimo trovato
                X = FMax[i];
                nX = X->ColID + X->RowID*(MFM->Cols);
                found = true;
            }
        }
        // Vado verso la posizione stimata
        CurrentPosition = GoTo(X,D_FMax, CurrentPosition);
        // Aggiorno posizione fake
        Declare();

        // Aggiorno FMax Fmin
        UpdateFrontier(&FMax);
        UpdateFrontier(&FMin);

    }

}


void AI::Declare()
{
    QList<Node*> F = FMin.length()>0 ? FMin: FMax;
    Node* D = FMin.length()>0 ?D_FMin: D_FMax;
    Node* X = F[0];
    int nX = X->ColID + X->RowID*(MFM->Cols);

    for(int i = 0; i < F.length(); i++){
        int nF = F[i]->ColID + F[i]->RowID*(MFM->Cols);
        int nD = D->ColID + D->RowID*(MFM->Cols);
        // Controllare se non e' grigia e se potrebbe essere un nuovo minimo di distanza dal goal direzione
        if(!F[i]->IsHidden
                && MFM->GMatrix->MinCost(nF, nD)<=MFM->GMatrix->MinCost(nX,nD))
                //&& d_x+d_y <= min_x+min_y)
        {
            // Aggiorno X con il nuovo minimo trovato
            X = F[i];
            nX = X->ColID + X->RowID*(MFM->Cols);
        }
    }
    // Vado verso la posizione stimata
    FakePosition = GoTo(X,D, FakePosition);
}


Node* AI::GoTo(Node* dest, Node* direction, Node* start)
{
    int *heur;
    Node* ret;
    
    if(direction==(*MFM->NodesMatrix)[0][0])
        heur = MFM->GMatrix->heur00;

    if(direction==(*MFM->NodesMatrix)[MFM->Rows-1][0])
        heur = MFM->GMatrix->heurR0;
    
    if(direction==(*MFM->NodesMatrix)[0][MFM->Cols-1])
        heur = MFM->GMatrix->heur0C;
    
    if(direction==(*MFM->NodesMatrix)[MFM->Rows-1][MFM->Cols-1])
        heur = MFM->GMatrix->heurRC;
    
    int next = MFM->GMatrix->BFS(
                start->ColID+start->RowID*(MFM->Cols),
                dest->ColID + dest->RowID*(MFM->Cols), heur);


    int col = next % (MFM->Cols);
    int row = next / (MFM->Cols);

    ret = (*MFM->NodesMatrix)[row][col];

    if((*MFM->NodesMatrix)[row][col] == dest)
        return ret;

    next = MFM->GMatrix->BFS(
                    ret->ColID+ret->RowID*(MFM->Cols),
                    dest->ColID + dest->RowID*(MFM->Cols), heur);

    col = next % (MFM->Cols);
    row = next / (MFM->Cols);
    ret = (*MFM->NodesMatrix)[row][col];
    return ret;
}


void AI::UpdateFrontier(QList<Node*>* F)
{
    QList<Node*> F_new;
    for (int i = 0; i<(*F).length(); i++) {

        //row-1,col-1
        if((*F)[i]->ColID-1>=0 &&
                (*F)[i]->RowID- 1 >=0 &&
                !(*MFM->NodesMatrix)[(*F)[i]->RowID-1][(*F)[i]->ColID-1]->IsDeleted)
        {
            if(!F_new.contains((*MFM->NodesMatrix)[(*F)[i]->RowID-1][(*F)[i]->ColID-1]))
                F_new.append((*MFM->NodesMatrix)[(*F)[i]->RowID-1][(*F)[i]->ColID-1]);
        }


        //row-1, col
        if((*F)[i]->RowID- 1 >=0 &&
                !(*MFM->NodesMatrix)[(*F)[i]->RowID-1][(*F)[i]->ColID]->IsDeleted)
        {
            if(!F_new.contains((*MFM->NodesMatrix)[(*F)[i]->RowID-1][(*F)[i]->ColID]))
                F_new.append((*MFM->NodesMatrix)[(*F)[i]->RowID-1][(*F)[i]->ColID]);
        }

        //row-1,col+1
        if((*F)[i]->ColID+1<MFM->Cols &&
                (*F)[i]->RowID- 1 >=0 &&
                !(*MFM->NodesMatrix)[(*F)[i]->RowID-1][(*F)[i]->ColID+1]->IsDeleted)
        {
            if(!F_new.contains((*MFM->NodesMatrix)[(*F)[i]->RowID-1][(*F)[i]->ColID+1]))
                F_new.append((*MFM->NodesMatrix)[(*F)[i]->RowID-1][(*F)[i]->ColID+1]);
        }
        //row, col-1
        if((*F)[i]->ColID-1>=0 &&
                !(*MFM->NodesMatrix)[(*F)[i]->RowID][(*F)[i]->ColID-1]->IsDeleted)
        {
            if(!F_new.contains((*MFM->NodesMatrix)[(*F)[i]->RowID][(*F)[i]->ColID-1]))
                F_new.append((*MFM->NodesMatrix)[(*F)[i]->RowID][(*F)[i]->ColID-1]);
        }

        //row, col+1
        if((*F)[i]->ColID+1<MFM->Cols &&
                !(*MFM->NodesMatrix)[(*F)[i]->RowID][(*F)[i]->ColID+1]->IsDeleted)
        {
            if(!F_new.contains((*MFM->NodesMatrix)[(*F)[i]->RowID][(*F)[i]->ColID+1]))
                F_new.append((*MFM->NodesMatrix)[(*F)[i]->RowID][(*F)[i]->ColID+1]);
        }

        //row+1, col-1
        if((*F)[i]->ColID-1>=0 &&
                (*F)[i]->RowID+1 <MFM->Rows &&
                !(*MFM->NodesMatrix)[(*F)[i]->RowID+1][(*F)[i]->ColID-1]->IsDeleted)
        {
            if(!F_new.contains((*MFM->NodesMatrix)[(*F)[i]->RowID+1][(*F)[i]->ColID-1]))
                F_new.append((*MFM->NodesMatrix)[(*F)[i]->RowID+1][(*F)[i]->ColID-1]);
        }

        //row+1, col
        if((*F)[i]->RowID+1 <MFM->Rows &&
                !(*MFM->NodesMatrix)[(*F)[i]->RowID+1][(*F)[i]->ColID]->IsDeleted)
        {
            if(!F_new.contains((*MFM->NodesMatrix)[(*F)[i]->RowID+1][(*F)[i]->ColID]))
                F_new.append((*MFM->NodesMatrix)[(*F)[i]->RowID+1][(*F)[i]->ColID]);
        }

        //row+1, col+1
        if((*F)[i]->ColID+1<MFM->Cols &&
                (*F)[i]->RowID+1 <MFM->Rows &&
                !(*MFM->NodesMatrix)[(*F)[i]->RowID+1][(*F)[i]->ColID+1]->IsDeleted)
        {
            if(!F_new.contains((*MFM->NodesMatrix)[(*F)[i]->RowID+1][(*F)[i]->ColID+1]))
                F_new.append((*MFM->NodesMatrix)[(*F)[i]->RowID+1][(*F)[i]->ColID+1]);
        }

    }
    F->clear();

    for(int i = 0; i < F_new.length(); i++)
    {
        F->append(F_new[i]);
    }
}

void AI::InvertMinMax(bool clearFmin)
{
    QList<Node*> FApp;
    for (int i = 0;i<FMax.length();i++)
    {
        FApp.append(FMax[i]);
    }

    FMax.clear();
    for (int i = 0;i<FMin.length();i++)
    {
        FMax.append(FMin[i]);
    }
    FMin.clear();
    for (int i = 0;i<FApp.length();i++)
    {
        FMin.append(FApp[i]);
    }

    // Inverto anche le direzioni e le loro code
    Node* app = D_FMin;
    D_FMin = D_FMax;
    D_FMax = app;

    FApp.clear();
    for (int i = 0;i<D_FMaxPoints.length();i++)
    {
        FApp.append(D_FMaxPoints[i]);
    }

    D_FMaxPoints.clear();
    for (int i = 0;i<D_FMinPoints.length();i++)
    {
        D_FMaxPoints.append(D_FMinPoints[i]);
    }
    D_FMinPoints.clear();
    for (int i = 0;i<FApp.length();i++)
    {
        D_FMinPoints.append(FApp[i]);
    }


    if(clearFmin)
    {
        FMin.clear();
        D_FMinPoints.clear();
    }


}


void AI::UpdateDirection(Node* direction, QList<Node*>* queue)
{
    if(queue->length()>qSqrt(qSqrt(MFM->Cols*MFM->Rows)))
    {
        // elimino 1
        queue->pop_back();
    }
    // Aggiungo 1
    queue->push_front(direction);

    // Faccio la media
    double avgx = 0,avgy = 0, dx = 0, dy = 0;


    for (int i = 0; i <queue->length(); i++) {
        avgx += (*queue)[i]->ColID;
        avgy += (*queue)[i]->RowID;
    }
    avgx /= queue->length();
    avgy /= queue->length();

    dx = (*queue)[0]->ColID - avgx;
    dy = (*queue)[0]->RowID - avgy;

    int r, c;

    if(dx > 0)
    {
        c = MFM->Cols-1;
    }
    else if (dx<0) {
        c =0;
    } else if (avgx >= MFM->Cols/2) {
        c = MFM->Cols-1;
    }
    else {
        c = 0;
    }

    if(dy > 0)
    {
        r = MFM->Rows-1;
    }
    else if (dy<0) {
        r =0;
    } else if (avgy >= MFM->Rows/2) {
        r = MFM->Rows-1;
    }
    else {
        r = 0;
    }

    if(queue == &D_FMaxPoints)
    {
        D_FMax = (*MFM->NodesMatrix)[r][c];
    }
    else
    {
        D_FMin = (*MFM->NodesMatrix)[r][c];
    }

}
