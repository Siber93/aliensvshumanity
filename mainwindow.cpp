#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "matrixfieldmanager.h"
#include "gameview.h"


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    int rows = ui->rowsTxt->toPlainText().toInt();
    int cols = ui->colsTxt->toPlainText().toInt();
    // Inizializzo la matrice
    MatrixFieldManager* mfm = new MatrixFieldManager();
    mfm->CreateMatrix(rows,
                      cols,
                      ui->hiddenTxt->toPlainText().toFloat(),
                      ui->emptyTxt->toPlainText().toFloat());
    // Creo il gestore delle sequenza di mosse nel gioco
    GameManager* gm = new GameManager(mfm);

    // Creo UI e AI
    AI* ai = new AI(ui->alienCb->isChecked() ? ALIEN:HUMAN, gm->MFM);
    GameView* gameV = new GameView(gm, ui->alienCb->isChecked() ? HUMAN:ALIEN);

    gm->ai = ai;
    // Avvio il gioco concedendo la prima mossa alla UI
    gameV->setModal(true);
    gameV->UpdateGrid();
    gameV->exec();


}
