#ifndef TILEBUTTON_H
#define TILEBUTTON_H

#include <QObject>
#include <QPushButton>

class TileButton : public QPushButton
{
    Q_OBJECT
public:

    int Row;
    int Col;


    explicit TileButton(int row, int col, QWidget *parent=nullptr);
    explicit TileButton(const QString &text, int row, int col, QWidget *parent= nullptr);

signals:

public slots:
};

#endif // TILEBUTTON_H
