#ifndef UTILS_H
#define UTILS_H


#include <QList>

// This class represents a directed graph using
// adjacency list representation
class Graph
{
    int V;    // No. of vertices

    // Pointer to an array containing adjacency
    // lists
    QList<int> *adj;


public:
    int *heur00, *heur0C, *heurR0, *heurRC;
    Graph(int V);  // Constructor

    // function to add an edge to graph
    void addEdge(int v, int w);

    void addHeur(int v, int val, int *heur);

    // prints BFS traversal from a given source s
    int BFS(int s, int d, int *heur);

    // Costo da un nodo all'altro
    int MinCost(int s, int d);
};

#endif // UTILS_H
