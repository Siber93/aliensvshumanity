#include "matrixfieldmanager.h"
#include <QtMath>
#include <QRandomGenerator>


MatrixFieldManager::MatrixFieldManager()
{
}

void MatrixFieldManager::CreateMatrix(int rows, int cols, float hiddenPerc, float emptyPerc)
{
    Rows= rows;
    Cols = cols;


    NodesMatrix = new QVector<QVector<Node*> >(rows, QVector<Node*>(cols));
    GMatrix = new Graph(rows*cols);

    // Generazione matrice rows X cols
    for (int i = 0; i < cols; i++)
    {
        for (int j = 0; j < rows; j++)
        {
            (*NodesMatrix)[j][i] = new Node(j,i);            
        }
    }

    // Eliminazione nodi randomicamente
    int numToDelete = (int)(rows * cols * emptyPerc);
    for (int k = 0; k<numToDelete ;k++)
    {
        int randRow = 0;
        int randCol = 0;
        // Continuo a cercare finche' non trovo un nodo pieno
        do {
            randRow =  QRandomGenerator::global()->bounded(rows);
            randCol =  QRandomGenerator::global()->bounded(cols);
        } while((*NodesMatrix)[randRow][randCol]->IsDeleted);        

        (*NodesMatrix)[randRow][randCol]->IsDeleted= true;
    }

    // Creo Grafo di navigazione
    for (int i = 0; i < cols; i++)
    {
        for (int j = 0; j < rows; j++)
        {
            // Calcolo funzione di euristica per navigazione nell albero A*
            if((*NodesMatrix)[j][i]->IsDeleted)
            {
                GMatrix->addHeur(i+j*cols, 0, GMatrix->heur00);
                GMatrix->addHeur(i+j*cols, 0, GMatrix->heurR0);
                GMatrix->addHeur(i+j*cols, 0, GMatrix->heur0C);
                GMatrix->addHeur(i+j*cols, 0, GMatrix->heurRC);
                continue;
            }else {
                int k1 = qAbs(j-i)+i*j;
                int k2 = qAbs(((rows-1)-j)-i)+i*((rows-1)-j);
                int k3 = qAbs(j-((cols-1)-i))+((cols-1)-i)*j;
                int k4 = (qAbs((rows-1)-(cols-1))+(rows-1)*(cols-1)) - k1;

                GMatrix->addHeur(i+j*cols, k1, GMatrix->heur00);
                GMatrix->addHeur(i+j*cols, k2, GMatrix->heurR0);
                GMatrix->addHeur(i+j*cols, k3, GMatrix->heur0C);
                GMatrix->addHeur(i+j*cols, k4, GMatrix->heurRC);
            }

            if(i-1>=0 && j- 1 >=0 && !(*NodesMatrix)[j-1][i-1]->IsDeleted)
            {
                GMatrix->addEdge(i+j*cols,(i-1)+(j-1)*cols);
            }
            if(j- 1 >=0 && !(*NodesMatrix)[j-1][i]->IsDeleted)
            {
                GMatrix->addEdge(i+j*cols,i+(j-1)*cols);
            }
            if(i+1<cols && j- 1 >=0 && !(*NodesMatrix)[j-1][i+1]->IsDeleted)
            {
                GMatrix->addEdge(i+j*cols,(i+1)+(j-1)*cols);
            }
            if(i-1>=0 && !(*NodesMatrix)[j][i-1]->IsDeleted)
            {
                GMatrix->addEdge(i+j*cols, (i-1)+j*cols);
            }
            if(i+1<cols && !(*NodesMatrix)[j][i+1]->IsDeleted)
            {
                GMatrix->addEdge(i+j*cols, (i+1)+j*cols);
            }
            if(i-1>=0 && j+1 < rows && !(*NodesMatrix)[j+1][i-1]->IsDeleted)
            {
                GMatrix->addEdge(i+j*cols, (i-1)+(j+1)*cols);
            }
            if(j+ 1 <rows && !(*NodesMatrix)[j+1][i]->IsDeleted)
            {
                GMatrix->addEdge(i+j*cols,i+(j+1)*cols);
            }
            if(i+1<cols && j+ 1 <rows && !(*NodesMatrix)[j+1][i+1]->IsDeleted)
            {
                GMatrix->addEdge(i+j*cols, (i+1)+(j+1)*cols);
            }

        }
    }


    // Imposto nodi a nascosto

    int numToHide = (int)(rows * cols * hiddenPerc);
    for (int k = 0; k<numToHide ;k++)
    {
        int randRow = 0;
        int randCol = 0;
        // Continuo a cercare finche' non trovo un nodo pieno e non nascosto
        do {
            randRow =  QRandomGenerator::global()->bounded(rows);
            randCol =  QRandomGenerator::global()->bounded(cols);
        } while((*NodesMatrix)[randRow][randCol]->IsDeleted ||
                (*NodesMatrix)[randRow][randCol]->IsHidden);

        (*NodesMatrix)[randRow][randCol]->MarkHidden();
    }

    // Imposto le basi
    int randRow = 0;
    int randCol = 0;
    randRow =  rows/4 + QRandomGenerator::global()->bounded(rows/2);
    randCol =  cols/4 + QRandomGenerator::global()->bounded(cols/2);

    (*NodesMatrix)[randRow][randCol]->MarkAsAlienBase();
    AlienHQ = (*NodesMatrix)[randRow][randCol];

    do {
        randRow =  rows/4 + QRandomGenerator::global()->bounded(rows/2);
        randCol =  cols/4 + QRandomGenerator::global()->bounded(cols/2);
    } while((*NodesMatrix)[randRow][randCol]->IsAlienBase);

    (*NodesMatrix)[randRow][randCol]->MarkAsHumanBase();
    HumanHQ = (*NodesMatrix)[randRow][randCol];

    // Imposto gli shuttle
    (*NodesMatrix)[0][0]->MarkAsShuttle();
    (*NodesMatrix)[0][cols-1]->MarkAsShuttle();
    (*NodesMatrix)[rows-1][0]->MarkAsShuttle();
    (*NodesMatrix)[rows-1][cols-1]->MarkAsShuttle();

}

bool MatrixFieldManager::IsTileAccessible(int row, int col)
{
    Node *n = (*NodesMatrix)[row][col];
    if (n->IsDeleted ||
        n->IsAlienBase||
        n->IsHumanBase)
    {
        return false;
    }
    return true;
}
