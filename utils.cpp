#include "utils.h"

Graph::Graph(int V)
{
    this->V = V;
    adj = new QList<int>[V];
    heur00 = new int[V];
    heurR0 = new int[V];
    heur0C = new int[V];
    heurRC = new int[V];
}

void Graph::addEdge(int v, int w)
{
    adj[v].push_back(w); // Add w to v’s list.
}

void Graph::addHeur(int v, int val, int *heur)
{
    heur[v] = val; // Add w to v’s list.
}



int Graph::MinCost(int s, int d)
{
    // Mark all the vertices as not visited
    bool *visited = new bool[V];
    for(int i = 0; i < V; i++)
        visited[i] = false;

    // Create a queue for BFS
    QList<int> queue;
    QList<int> queueNextStep;

    // Mark the current node as visited and enqueue it
    visited[s] = true;
    queue.push_back(s);
    queueNextStep.push_back(0);



    // 'i' will be used to get all adjacent
    // vertices of a vertex
    QList<int>::iterator i;
    int padre;
    while(!queue.empty())
    {
        //init = false;
        // Dequeue a vertex from queue and print it
        s = queue.front();
        queue.pop_front();

        padre = queueNextStep.front();
        queueNextStep.pop_front();


        // Get all adjacent vertices of the dequeued
        // vertex s. If a adjacent has not been visited,
        // then mark it visited and enqueue it
        for (i = adj[s].begin(); i != adj[s].end(); ++i)
        {
            if (!visited[*i])
            {
                visited[*i] = true;
                queue.push_back(*i);
                queueNextStep.push_back(padre+1);

                // Verifico se ho trovato il nodo destinazione
                if(*i == d )
                {
                    return padre + 1;
                }
            }
        }
    }
    return 1000000;
}


int Graph::BFS(int s, int d, int *heur)
{

    // Create a queue for BFS
    QList<int> queue;
    QList<int> queueNextStep;
    QList<int> queueNumStep;

    // Mark the current node as visited and enqueue it
    queue.push_back(s);
    queueNumStep.push_back(0);


    int heurMin = -1;
    int nodeHeur = s;
    int level = -1;

    // 'i' will be used to get all adjacent
    // vertices of a vertex
    QList<int>::iterator i;
    // Init impostato al numero di mosse possibili nel movimento (alieno 2 umano 1)
    bool init = true;
    int padre;
    int numPadre;
    while(!queue.empty())
    {
        //init = false;
        // Dequeue a vertex from queue and print it
        s = queue.front();        
        queue.pop_front();

        // Dequeu number deep level of the father
        numPadre = queueNumStep.front();
        queueNumStep.pop_front();

        if(!queueNextStep.isEmpty())
        {
            padre = queueNextStep.front();
            queueNextStep.pop_front();
            init = false;
        }

        // Get all adjacent vertices of the dequeued
        // vertex s. If a adjacent has not been visited,
        // then mark it visited and enqueue it
        for (i = adj[s].begin(); i != adj[s].end(); ++i)
        {

            queue.push_back(*i);
            queueNumStep.push_back(numPadre+1);
            if(init)
            {
                queueNextStep.push_back(*i);
                padre = *i;
            }
            else{
                queueNextStep.push_back(padre);
            }

            // Se trovo per la prima volta il nodo destinazione mi savlo il livello al quale l ho trovato
            if(*i == d && level == -1)
            {
                level = numPadre+1;
                nodeHeur = padre;
                heurMin = heur[*i];
            }
            else if(*i == d && numPadre+1==level && heurMin>heur[*i])
            {
                // Se trovo nuovamente il nodo destinazione allo stesso livello di quello precedente e con una euristica migliore melo salvo
                nodeHeur = padre;
                heurMin = heur[*i];
            }

            // Se ho gia' trovato il nodo destinazione e ho cambiato livello ritorno
            if(level > -1 && padre > level)
            {
                return nodeHeur;
            }

        }
    }
    return nodeHeur;
}
